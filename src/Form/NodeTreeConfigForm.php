<?php

namespace Drupal\node_tree\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;

/**
 * Class NodeTreeConfigForm
 * @package Drupal\node_tree\Form
 */
class NodeTreeConfigForm extends ConfigFormBase {
  /**
   * @var string
   *
   * Name of the field.
   */
  const NODE_TREE_FIELD = 'field_node_tree_parent_node';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_tree.config',
    ];
  }

  /**
   * Get all the bundle names for the node entity.
   *
   * @return array
   */
  private function getAllBundleNames() {
    $bundle_types = NodeType::loadMultiple();
    $bundle_names = [];

    // Get all the labels.
    foreach ($bundle_types as $bundle_key => $bundle_value) {
      $bundle_names[$bundle_key] = $bundle_value->label();
    }

    return $bundle_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_tree.config');

    $form['node_tree_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#description' => $this->t('Select which content types are managed by this module.<br>If enabled, a field named <i>@field_name</i> will be created in corresponding content type.<br>Please <strong>do not</strong> modify/delete this field manually.', ['@field_name' => self::NODE_TREE_FIELD]),
      '#options' => $this->getAllBundleNames(),
      '#default_value' => $config->get('node_tree_content_types') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws InvalidPluginDefinitionException
   * @throws EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get what user sent through the form.
    $fields['bundle_types'] = (array) $form_state->getValue('node_tree_content_types');

    // Definition of the entity reference field that handles hierarchy in tree.
    $fields['fields'] = [
      self::NODE_TREE_FIELD => [
        'type' => 'entity_reference',
        'entity_type' => 'node',
        'title' => t('[Node tree] Parent node'),
        'label' => t('[Node tree] Parent node'),
        'required' => FALSE,
        'description' => t('The parent node in hierarchical tree management.'),
        'settings' => [
          'handler' => 'default:node',
          'handler_settings' => [
            // The bundles that can be referenced are by default all bundles.
            'target_bundles' => $fields['bundle_types'],
          ]
        ]
      ]
    ];

    foreach ($fields['fields'] as $field_name => $field_config) {
      $field_storage = FieldStorageConfig::loadByName($field_config['entity_type'], $field_name);

      if (empty($field_storage)) {
        FieldStorageConfig::create([
          'field_name' => $field_name,
          'entity_type' => $field_config['entity_type'],
          'type' => $field_config['type'],
        ])->save();
      }
    }

    foreach ($fields['bundle_types'] as $bundle_key => $bundle_value) {
      if ($bundle_value) {
        // The field needs to be created.
        foreach ($fields['fields'] as $field_name => $field_config) {
          $config_array = [
            'field_name' => $field_name,
            'entity_type' => $field_config['entity_type'],
            'bundle' => $bundle_key,
            'label' => $field_config['label'],
            'required' => $field_config['required'],
          ];

          if (isset($field_config['settings'])) {
            $config_array['settings'] = $field_config['settings'];
          }

          $field = FieldConfig::loadByName($field_config['entity_type'], $bundle_key, $field_name);

          if (empty($field) && $bundle_key !== '' && !empty($bundle_key)) {
            FieldConfig::create($config_array)->save();
          }

          if ($bundle_key !== '' && !empty($bundle_key) && !empty($field)) {
            $field
              ->setLabel($field_config['label'])
              ->save();

            $field
              ->setRequired($field_config['required'])
              ->save();
          }
        }
      }
      else {
        // The field needs to be deleted.
        foreach ($fields['fields'] as $field_name => $field_config) {
          $field = FieldConfig::loadByName($field_config['entity_type'], $bundle_key, $field_name);

          if ($field) {
            $field->delete();
          }
        }
      }
    }

    // Save the configuration.
    $this->config('node_tree.config')
      ->set('node_tree_content_types', $fields['bundle_types'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_tree_config_form';
  }
}
