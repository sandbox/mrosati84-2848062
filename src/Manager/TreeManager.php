<?php
/**
 * Node Tree Manager
 */
namespace Drupal\node_tree\Manager;

use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\node\Entity\Node;
use Drupal\node_tree\Form\NodeTreeConfigForm;
use Symfony\Component\Serializer\Serializer;

/**
 * Class TreeManager
 *
 * @package Drupal\node_tree\Manager
 */
class TreeManager {

  protected $query_factory;
  protected $serializer;

  public function __construct(QueryFactory $queryFactory, Serializer $serializer)
  {
    $this->query_factory = $queryFactory;
    $this->serializer = $serializer;
  }

  /**
   * Generates a json representing the nodes tree using the JsTree synthax
   *  Ex.
      { "id" : "ajson1", "parent" : "#", "text" : "Simple root node" },
      { "id" : "ajson2", "parent" : "#", "text" : "Root node 2" },
      { "id" : "ajson3", "parent" : "ajson2", "text" : "Child 1" },
      { "id" : "ajson4", "parent" : "ajson2", "text" : "Child 2" }
   */
  public function generateTreeJson()
  {
    //$this->query_factory;
    $nodes_ids = $this->query_factory->get('node')
      //->condition(NodeTreeConfigForm::NODE_TREE_FIELD, NULL, 'IS NOT NULL')
      //->sort(NodeTreeConfigForm::NODE_TREE_FIELD, 'ASC')
      ->sort('nid', 'ASC')
      ->execute();

    $nodes = Node::loadMultiple($nodes_ids);
    $formatted_output = array();
    foreach($nodes as $i => $node) {

      if($node->hasField(NodeTreeConfigForm::NODE_TREE_FIELD)) {

        $res = '#';

        if($node->get(NodeTreeConfigForm::NODE_TREE_FIELD)->getString()) {
          $res = $node->get(NodeTreeConfigForm::NODE_TREE_FIELD)->getString();
        }

        $formatted_output[] = array(
          'id' => $node->nid->getString(),
          'parent' => $res,
          'text' => $node->title->getString()
        );
      }
    }

    return $this->serializer->serialize(
      $formatted_output,
      'json'
      //['plugin_id' => 'entity']
    );
  }
}
