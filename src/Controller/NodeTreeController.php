<?php

namespace Drupal\node_tree\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node_tree\Manager\TreeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeTreeController extends ControllerBase {

  protected $tree_manager;

  public function __construct(TreeManager $treeManager)
  {
    $this->tree_manager = $treeManager;
  }

  /**
   * Services definition
   *
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    $tree_manager = $container->get('node_tree.tree_manager');

    return new static($tree_manager);
  }

  public function tree() {

    $nodes_tree = $this->tree_manager->generateTreeJson();
    return [
      '#nodes_tree' => $nodes_tree,
      '#theme' => 'tree',
      '#attached' => [
        'library' => [
          'node_tree/node_tree',
        ],
        'drupalSettings' => [
          'nodes_tree' => $nodes_tree
        ]
      ],
    ];
  }

  public function configure() {
    return [
      '#markup' => 'Node tree configuration.',
    ];
  }
}
